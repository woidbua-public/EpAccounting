﻿// ///////////////////////////////////
// File: DatabaseFactory.cs
// Last Change: 31.08.2018 19:10
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.Test
{
    using System;
    using System.IO;
    using EpAccounting.UI.Properties;

    public static class DatabaseFactory
    {
        #region Fields & Constants

        private const string DirectoryFoldername = "Test - EpAccounting";

        #endregion



        #region Properties & Indexers

        public static string TestFolderPath => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), DirectoryFoldername);

        public static string TestFilePath => Path.Combine(TestFolderPath, Resources.Database_NameWithExtension);

        #endregion



        #region Methods

        public static void CreateTestFolder()
        {
            if (Directory.Exists(TestFolderPath))
            {
                return;
            }

            Directory.CreateDirectory(TestFolderPath);
        }

        public static void CreateTestFile()
        {
            CreateTestFolder();

            if (File.Exists(TestFilePath))
            {
                File.Delete(TestFilePath);
            }

            FileStream fileStream = File.Create(TestFilePath);
            fileStream.Close();
        }

        public static void DeleteTestFolderAndFile()
        {
            if (!Directory.Exists(TestFolderPath))
            {
                return;
            }

            DirectoryInfo directoryInfo = new DirectoryInfo(TestFolderPath);
            directoryInfo.Delete(true);
        }

        public static void SetSavedFilePath()
        {
            Settings.Default.DatabaseFilePath = TestFilePath;
            Settings.Default.Save();
        }

        public static void ClearSavedFilePath()
        {
            Settings.Default.DatabaseFilePath = string.Empty;
            Settings.Default.Save();
        }

        #endregion
    }
}