﻿// ///////////////////////////////////
// File: KindOfBill.cs
// Last Change: 31.08.2018 07:28
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.Model.Enum
{
    using System.ComponentModel;
    using Converter;

    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum KindOfBill
    {
        [Description("Rechnung")]
        Rechnung,

        [Description("Angebot")]
        Angebot,

        [Description("Kostenvoranschlag")]
        Kostenvoranschlag,

        [Description("Gutschrift")]
        Gutschrift,

        [Description("Bestätigung")]
        Bestaetigung
    }
}