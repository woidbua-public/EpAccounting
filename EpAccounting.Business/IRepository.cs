﻿// ///////////////////////////////////
// File: IRepository.cs
// Last Change: 10.11.2018 14:15
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.Business
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using NHibernate.Criterion;

    public interface IRepository
    {
        #region Properties & Indexers

        bool IsConnected { get; }

        string FilePath { get; }

        #endregion



        #region Methods

        void CreateDatabase(string filePath);

        void LoadDatabase(string filePath);

        void CloseDatabase();

        void SaveOrUpdate<T>(T t) where T : class;

        void Delete<T>(T t) where T : class;

        int GetQuantity<T>() where T : class;

        ICollection<T> GetAll<T>(int page, Expression<Func<T, object>> orderCriterion = null) where T : class;

        T GetById<T>(int id) where T : class;

        int GetQuantityByCriteria<T>(ICriterion criterion) where T : class;

        int GetQuantityByCriteria<T, TU>(ICriterion criterion1,
                                         Expression<Func<T, TU>> combinationCriterion,
                                         ICriterion criterion2) where T : class;

        int GetQuantityByCriteria<T, TU, TV>(ICriterion criterion1,
                                             Expression<Func<T, TU>> combinationCriterion1,
                                             ICriterion criterion2,
                                             Expression<Func<TU, TV>> combinationCriterion2,
                                             ICriterion criterion3) where T : class;

        ICollection<T> GetByCriteria<T>(ICriterion criterion, int page, Expression<Func<T, object>> orderCriterion = null) where T : class;

        ICollection<T> GetByCriteria<T, TU>(ICriterion criterion1,
                                            Expression<Func<T, TU>> combinationCriterion,
                                            ICriterion criterion2,
                                            int page,
                                            Expression<Func<T, object>> orderCriterion = null) where T : class;

        ICollection<T> GetByCriteria<T, TU, TV>(ICriterion criterion1,
                                                Expression<Func<T, TU>> combinationCriterion1,
                                                ICriterion criterion2,
                                                Expression<Func<TU, TV>> combinationCriterion2,
                                                ICriterion criterion3,
                                                int page,
                                                Expression<Func<T, object>> orderCriterion = null) where T : class;

        #endregion
    }
}