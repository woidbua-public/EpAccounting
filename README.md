<!-- markdownlint-disable MD030-->

# EpAccounting

## Description

Management of Clients & Bills.

## Tasks

-   Save Clients & Bills in SQLite Database
-   Automate the creation of bills with following print process
-   Client & Bill Search ability

## Impressions

### Client Menu

Management of clients (Search, Add, Edit, Delete).
![Client Menu](docs/img/epa_1.png)

### Bill Menu

Management of bills (Search, Add, Edit, Delete).
![Bill Menu](docs/img/epa_2.png)

### Options Menu

Management of file paths and global settings.
![Options Menu](docs/img/epa_3.png)
