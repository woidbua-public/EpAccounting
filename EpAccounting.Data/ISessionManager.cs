﻿// ///////////////////////////////////
// File: ISessionManager.cs
// Last Change: 31.08.2018 19:02
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.Data
{
    using NHibernate;

    public interface ISessionManager
    {
        #region Properties & Indexers

        bool IsConnected { get; }

        string FilePath { get; }

        #endregion



        #region Methods

        void CreateDatabase(string folderPath);

        void LoadDatabase(string filePath);

        void CloseDatabase();

        ISession OpenSession();

        #endregion
    }
}