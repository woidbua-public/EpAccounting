﻿// ///////////////////////////////////
// File: EnumDescriptionExtension.cs
// Last Change: 31.08.2018 20:01
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.Markup
{
    using System;
    using System.ComponentModel;

    public static class EnumDescriptionExtension
    {
        #region Methods

        public static string GetDescription<T>(this T enumerationValue) where T : IConvertible
        {
            var type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                // ReSharper disable once LocalizableElement
                throw new ArgumentException($"{nameof(enumerationValue)} must be of Enum type", nameof(enumerationValue));
            }

            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length <= 0)
            {
                return enumerationValue.ToString();
            }

            var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attrs.Length > 0 ? ((DescriptionAttribute) attrs[0]).Description : enumerationValue.ToString();
        }

        #endregion
    }
}