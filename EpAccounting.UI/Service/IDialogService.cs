﻿// ///////////////////////////////////
// File: IDialogService.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.Service
{
    using System;
    using System.Drawing.Printing;
    using System.Threading.Tasks;

    public interface IDialogService
    {
        #region Methods

        Task ShowMessage(string title, string message);

        Task ShowExceptionMessage(Exception e, string title = "An error occured!");

        Task<bool> ShowDialogYesNo(string title, string message);

        Task<bool> ShowCustomDialog(string title, string message, string button1, string button2);

        string ShowDatabaseFileDialog();

        string ShowFolderDialog();

        void ShowPrintDialog(PrintDocument document);

        #endregion
    }
}