﻿// ///////////////////////////////////
// File: IWordService.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.Service
{
    using ViewModel;

    public interface IWordService
    {
        #region Methods

        void CreateWordBill(BillItemEditViewModel billItemEditViewModel, bool visible);

        bool PrintDocument();

        void CloseDocument();

        #endregion
    }
}