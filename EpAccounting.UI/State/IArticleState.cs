﻿// ///////////////////////////////////
// File: IArticleState.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.State
{
    public interface IArticleState
    {
        #region Methods

        bool CanSwitchToEditMode();

        void SwitchToEditMode();

        bool CanCommit();

        void Commit();

        bool CanCancel();

        void Cancel();

        #endregion
    }
}