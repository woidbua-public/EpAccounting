﻿// ///////////////////////////////////
// File: IClientState.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.State
{
    using System.Threading.Tasks;

    public interface IClientState
    {
        #region Methods

        bool CanSwitchToSearchMode();

        void SwitchToSearchMode();

        bool CanSwitchToAddMode();

        void SwitchToAddMode();

        bool CanSwitchToEditMode();

        void SwitchToEditMode();

        bool CanCommit();

        Task Commit();

        bool CanCancel();

        void Cancel();

        bool CanDelete();

        Task Delete();

        #endregion
    }
}