﻿// ///////////////////////////////////
// File: ArticleViewModel.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.ViewModel
{
    using System;
    using Business;
    using Model;
    using Service;

    public class ArticleViewModel : BindableViewModelBase
    {
        #region Fields & Constants

        private readonly Article _article;
        private readonly IDialogService _dialogService;
        private readonly IRepository _repository;

        #endregion



        #region Constructors

        public ArticleViewModel(Article article, IRepository repository, IDialogService dialogService)
        {
            this._article = article;
            this._repository = repository;
            this._dialogService = dialogService;
        }

        #endregion



        #region Properties & Indexers

        public int Id => this._article.Id;

        public int ArticleNumber
        {
            get => this._article.ArticleNumber;
            set { this.SetProperty(() => this._article.ArticleNumber = value, () => this._article.ArticleNumber == value); }
        }

        public string Description
        {
            get => this._article.Description;
            set { this.SetProperty(() => this._article.Description = value, () => this._article.Description == value); }
        }

        public double Amount
        {
            get => this._article.Amount;
            set { this.SetProperty(() => this._article.Amount = value, () => Math.Abs(this._article.Amount - value) < 0.01); }
        }

        public decimal Price
        {
            get => this._article.Price;
            set { this.SetProperty(() => this._article.Price = value, () => this._article.Price == value); }
        }

        #endregion



        #region Methods

        public void Save()
        {
            try
            {
                this._repository.SaveOrUpdate(this._article);
            }
            catch (Exception e)
            {
                this._dialogService.ShowExceptionMessage(e, $"Could not save article '{this._article.Description}'");
            }
        }

        #endregion
    }
}