// ///////////////////////////////////
// File: ViewModelLocator.cs
// Last Change: 31.08.2018 19:47
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.ViewModel
{
    using Business;
    using Data;
    using GalaSoft.MvvmLight.Ioc;
    using Microsoft.Practices.ServiceLocation;
    using Service;

    public class ViewModelLocator
    {
        #region Constructors

        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<ISessionManager, NHibernateSessionManager>();
            SimpleIoc.Default.Register<IRepository, NHibernateRepository>();
            SimpleIoc.Default.Register<IDialogService, DialogService>();
            SimpleIoc.Default.Register<MainViewModel>();
        }

        #endregion



        #region Properties & Indexers

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();

        #endregion



        #region Methods

        public static void Cleanup()
        {
            // Clear the ViewModels
        }

        #endregion
    }
}