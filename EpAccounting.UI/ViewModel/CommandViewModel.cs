﻿// ///////////////////////////////////
// File: CommandViewModel.cs
// Last Change: 31.08.2018 19:47
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.ViewModel
{
    using GalaSoft.MvvmLight.Command;

    public class CommandViewModel : BindableViewModelBase
    {
        #region Constructors

        public CommandViewModel(string displayName, RelayCommand relayCommand)
        {
            this.DisplayName = displayName;
            this.RelayCommand = relayCommand;
        }

        #endregion



        #region Properties & Indexers

        public string DisplayName { get; }

        public RelayCommand RelayCommand { get; }

        #endregion
    }
}