﻿// ///////////////////////////////////
// File: ImageCommandViewModel.cs
// Last Change: 31.08.2018 19:47
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.ViewModel
{
    using System.Drawing;
    using GalaSoft.MvvmLight.Command;

    public class ImageCommandViewModel : CommandViewModel
    {
        #region Constructors

        public ImageCommandViewModel(Bitmap image, string displayName, RelayCommand relayCommand)
            : base(displayName, relayCommand)
        {
            this.Image = image;
        }

        #endregion



        #region Properties & Indexers

        public Bitmap Image { get; set; }

        #endregion
    }
}