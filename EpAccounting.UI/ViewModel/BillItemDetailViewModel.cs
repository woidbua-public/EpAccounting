﻿// ///////////////////////////////////
// File: BillItemDetailViewModel.cs
// Last Change: 31.08.2018 19:47
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.ViewModel
{
    using System;
    using System.Linq;
    using Business;
    using Model;
    using Model.Enum;
    using NHibernate.Criterion;

    public class BillItemDetailViewModel : BindableViewModelBase
    {
        #region Fields & Constants

        private readonly BillItem _billItem;
        private readonly IRepository _repository;

        #endregion



        #region Constructors

        public BillItemDetailViewModel(BillItem billItem, IRepository repository)
        {
            this._billItem = billItem;
            this._repository = repository;
        }

        #endregion



        #region Properties & Indexers

        public int Id => this._billItem.Id;

        public int Position
        {
            get => this._billItem.Position;
            set { this.SetProperty(() => this._billItem.Position = value, () => this._billItem.Position == value); }
        }

        public int ArticleNumber
        {
            get => this._billItem.ArticleNumber;
            set
            {
                if (this.SetProperty(() => this._billItem.ArticleNumber = value, () => this._billItem.ArticleNumber == value))
                {
                    this.FillArticleValues();
                }
            }
        }

        public string Description
        {
            get => this._billItem.Description;
            set { this.SetProperty(() => this._billItem.Description = value, () => this._billItem.Description == value); }
        }

        public double Amount
        {
            get => this._billItem.Amount;
            set
            {
                this.SetProperty(() => this._billItem.Amount = value, () => Math.Abs(this._billItem.Amount - value) < 0.01);
                this.RaisePropertyChanged(() => this.Sum);
            }
        }

        public decimal Price
        {
            get => this._billItem.Price;
            set
            {
                this.SetProperty(() => this._billItem.Price = value, () => this._billItem.Price == value);
                this.RaisePropertyChanged(() => this.Sum);
            }
        }

        public double Discount
        {
            get => this._billItem.Discount;
            set
            {
                this.SetProperty(() => this._billItem.Discount = value, () => Math.Abs(this._billItem.Discount - value) < 0.01);
                this.RaisePropertyChanged(() => this.Sum);
            }
        }

        public decimal Sum => (decimal) this._billItem.Amount * this._billItem.Price / 100 * (100 - (decimal) this._billItem.Discount);

        #endregion



        #region Methods

        private void FillArticleValues()
        {
            Article article = null;

            try
            {
                article = this._repository.GetByCriteria<Article>(Restrictions.Where<Article>(x => x.ArticleNumber == this.ArticleNumber), 1).FirstOrDefault();
            }
            catch
            {
                // do nothing
            }

            if (article != null)
            {
                this.Description = article.Description;
                this.Amount = article.Amount;

                if (this._billItem.Bill.KindOfVat == KindOfVat.inkl_MwSt)
                {
                    this.Price = article.Price * (100 + (decimal) this._billItem.Bill.VatPercentage) / 100;
                }
                else
                {
                    this.Price = article.Price;
                }
            }
        }

        #endregion
    }
}