﻿// ///////////////////////////////////
// File: MainWindow.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View
{
    using MahApps.Metro.Controls;

    public partial class MainWindow : MetroWindow
    {
        #region Constructors

        public MainWindow()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}