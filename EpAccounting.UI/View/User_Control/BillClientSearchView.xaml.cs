﻿// ///////////////////////////////////
// File: BillClientSearchView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for BillClientSearchView.xaml
    /// </summary>
    public partial class BillClientSearchView : UserControl
    {
        #region Constructors

        public BillClientSearchView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}