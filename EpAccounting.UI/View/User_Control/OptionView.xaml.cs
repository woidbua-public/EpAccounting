﻿// ///////////////////////////////////
// File: OptionView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    public partial class OptionView : UserControl
    {
        #region Constructors

        public OptionView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}