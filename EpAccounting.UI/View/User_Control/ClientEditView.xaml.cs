﻿// ///////////////////////////////////
// File: ClientEditView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for ClientEditView.xaml
    /// </summary>
    public partial class ClientEditView : UserControl
    {
        #region Constructors

        public ClientEditView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}