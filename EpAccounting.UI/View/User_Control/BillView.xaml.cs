﻿// ///////////////////////////////////
// File: BillView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for BillView.xaml
    /// </summary>
    public partial class BillView : UserControl
    {
        #region Constructors

        public BillView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}