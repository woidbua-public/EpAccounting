﻿// ///////////////////////////////////
// File: ClientSearchView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for ClientSearchView.xaml
    /// </summary>
    public partial class ClientSearchView : UserControl
    {
        #region Constructors

        public ClientSearchView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}