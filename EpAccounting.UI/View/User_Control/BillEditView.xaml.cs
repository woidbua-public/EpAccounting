﻿// ///////////////////////////////////
// File: BillEditView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for BillEditView.xaml
    /// </summary>
    public partial class BillEditView : UserControl
    {
        #region Constructors

        public BillEditView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}