﻿// ///////////////////////////////////
// File: ClientView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for ClientView.xaml
    /// </summary>
    public partial class ClientView : UserControl
    {
        #region Constructors

        public ClientView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}