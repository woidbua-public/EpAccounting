﻿// ///////////////////////////////////
// File: BillSearchView.xaml.cs
// Last Change: 31.08.2018 19:46
// Author: Andre Multerer
// ///////////////////////////////////

namespace EpAccounting.UI.View.User_Control
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for BillSearchView.xaml
    /// </summary>
    public partial class BillSearchView : UserControl
    {
        #region Constructors

        public BillSearchView()
        {
            this.InitializeComponent();
        }

        #endregion
    }
}